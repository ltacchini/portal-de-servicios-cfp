# cfp
Web app hecha en drupal 8 para el CFP 403


## Instalación de Drupal (development environment)

### Instalar y correr apache2

```
sudo apt update
sudo apt update
sudo apt install apache2
sudo systemctl stop apache2.service
sudo systemctl start apache2.service
sudo systemctl enable apache2.service
```


### Instalar y correr mysql
```
sudo apt-get install mariadb-server mariadb-client
sudo systemctl stop mariadb.service
sudo systemctl start mariadb.service
sudo systemctl enable mariadb.service
sudo mysql_secure_installation 
Contraseña root, todo y el resto.
sudo mysql -u root -p
```

### Instalar PHP
```
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install php7.2 libapache2-mod-php7.2 php7.2-common php7.2-mbstring php7.2-xmlrpc php7.2-soap php7.2-gd php7.2-xml php7.2-intl php7.2-mysql php7.2-cli php7.2-zip php7.2-curl
sudo apt-get update
sudo apt-get upgrade
sudo nano /etc/php/7.2/apache2/php.ini
```

Cambiar estas variables:
```
file_uploads = On
allow_url_fopen = On
memory_limit = 256M
upload_max_filesize = 100M
max_execution_time = 360
date.timezone = America/Chicago
      8. sudo nano /var/www/html/phpinfo.php
         escribir en el archivo: <?php phpinfo( ); ?>
      10. Entrar a: http://localhost/phpinfo.php
```

### Instalar Drupal

Descargar drupal de la pagina oficial https://www.drupal.org/project/drupal/releases/8.5.6

Instalar composer siguiendo esta guia: 
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-18-04

```
composer create-project drupal/drupal 
composer install
```
